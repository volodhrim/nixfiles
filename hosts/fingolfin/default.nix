{ pkgs, config, lib, ... }:
{
  imports = [
    ../home.nix
    ./hardware-configuration.nix
  ];

  ## Modules
  modules = {
    desktop = {
      bspwm.enable = true;
      apps = {
        discord.enable = true;
        rofi.enable = true;
        # godot.enable = true;
        signal.enable = true;
        dolphin.enable = true;
        telegram.enable = true;
        slack.enable = true;
        nheko.enable = true;
      };
      browsers = {
        default = "firefox";
        # chromium.enable = true;
        firefox.enable = true;
        #qutebrowser.enable = true;
      };
      gaming = {
        # steam.enable = true;
        # emulators.enable = true;
        # emulators.psx.enable = true;
      };
      media = {
        daw.enable = true;
        ncmpcpp.enable = true;
        documents = {
          enable = true;
          pdf.enable = true;
          office.enable = true;
        };
        graphics = {
          enable = true;
          models.enable = false;
          sprites.enable = false;
          vector.enable = false;
        };
        mpv.enable = true;
        recording.enable = true;
        spotify.enable = true;
      };
      term = {
        st.enable = true;
        alacritty.enable = true;
        default = "alacritty";
      };
      vm = {
        #qemu.enable = true;
      };
    };
    dev = {
      cc.enable = true;
      clojure.enable = true;
      go.enable = true;
      node.enable = true;
      ops.enable = true;
    };
    editors = {
      default = "nvim";
      emacs.enable = true;
      vim.enable = true;
    };
    shell = {
      adl.enable = true;
      #bitwarden.enable = true;
      direnv.enable = true;
      git.enable    = true;
      gnupg.enable  = true;
      tmux.enable   = true;
      zsh.enable    = true;
      ranger.enable = true;
    };
    services = {
      ssh.enable = true;
      ftp.enable = true;
      docker.enable = true;
      greenclip.enable = true;
      # Needed occasionally to help the parental units with PC problems
      # teamviewer.enable = true;
    };
    theme.active = "alucard";
  };


  ## Local config
  programs.ssh.startAgent = true;
  services.openssh.startWhenNeeded = true;

  services.xserver = {
    layout = "us,ru";
    #xkbVariant = "colemak,";
  };
  networking = {
    wireless.userControlled.enable = true;
    networkmanager.enable = true;
    # The global useDHCP flag is deprecated, therefore explicitly set to false
    # here. Per-interface useDHCP will be mandatory in the future, so this
    # generated config replicates the default behaviour.
    useDHCP = false;
    usePredictableInterfaceNames = false; # fuck that systemd shit
  };

  ## Personal backups
  # TODO set it up with a separate HDD
}
