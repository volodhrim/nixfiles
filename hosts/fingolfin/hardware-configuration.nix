{ config, lib, pkgs, modulesPath, ... }:

{
  boot = {
    initrd = {
      luks.devices = {
        root = {
          device = "/dev/disk/by-uuid/c11b5f47-7580-4f95-86f6-098d43509746";
          preLVM = true;
          allowDiscards = true;
        };
      };
      availableKernelModules = [
        "ahci"
        "nvme"
        "rtsx_pci_sdmmc"
        "sd_mod"
        "usb_storage"
        "usbhid"
        "xhci_pci"
      ];
    };
    initrd.kernelModules = [ "dm-snapshot" ];
    kernelModules = [
      "kvm-intel"
    ];
    loader = {
      efi.canTouchEfiVariables = true;
      systemd-boot.enable = false;
      grub = {
        enable = true;
        device = "nodev";
        efiSupport = true;
      };
    };

  };

  # Modules
  modules.hardware = {
    audio.enable = true;
    fs = {
      enable = true;
      ssd.enable = true;
    };
    ergodox.enable = true;
    nvidia.enable = true;
    sensors.enable = true;
  };

  # CPU
  nix.settings.max-jobs = lib.mkDefault 8;
  powerManagement.cpuFreqGovernor = "powersave";


  # Storage
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/83695ed0-6f45-47f1-845b-f755d968703d";
      fsType = "btrfs";
      options = [ "noatime" "nodiratime" "discard" ];
    };
  };


  swapDevices =
    [ { device = "/dev/disk/by-uuid/aa28684c-40f5-43cc-a28a-86bca3ca21a1"; }
    ];

  hardware = {
    # high-resolution display
    video.hidpi.enable = lib.mkDefault true;
    enableRedistributableFirmware = true;
  };
}
