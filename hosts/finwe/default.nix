{ pkgs, config, lib, ... }:
{
  imports = [
    ../home.nix
    ./hardware-configuration.nix
  ];

  ## Modules
  modules = {
    hardware ={
      android.enable = true;
    };
    networking.enable = true;
    desktop = {
      #bspwm.enable = true;
      kde.enable = true;
      apps = {
        discord.enable = true;
        rofi.enable = true;
        teams.enable = true;
        # godot.enable = true;
        signal.enable = true;
        dolphin.enable = true;
        telegram.enable = true;
        #nheko.enable = true;
      };
      browsers = {
        default = "firefox";
        # chromium.enable = true;
        firefox.enable = true;
        qutebrowser.enable = true;
      };
      gaming = {
        steam.enable = true;
        # emulators.enable = true;
        # emulators.psx.enable = true;
      };
      media = {
        #daw.enable = true;
        autorandr.enable = true;
        ncmpcpp.enable = true;
        documents = {
          enable = true;
          pdf.enable = true;
          office.enable = true;
        };
        graphics.enable = true;
        mpv.enable = true;
        recording.enable = true;
        #spotify.enable = true;
      };
      term = {
        st.enable = true;
        alacritty.enable = true;
        default = "alacritty";
      };
      vm = {
        qemu.enable = true;
        virtualbox.enable = true;
      };
    };
    dev = {
      cc.enable = true;
      clojure.enable = true;
      common-lisp.enable = true;
      go.enable = true;
      haskell.enable = true;
      node.enable = true;
      ops.enable = true;
      racket.enable = true;
      rust.enable = true;
      shell.enable = true;
    };
    editors = {
      default = "vim";
      emacs.enable = true;
      vim.enable = true;
    };
    shell = {
      #adl.enable = true;
      #bitwarden.enable = true;
      direnv.enable = true;
      git.enable    = true;
      gnupg.enable  = true;
      ranger.enable = true;
      tmux.enable   = true;
      zsh.enable    = true;
    };
    services = {
      ssh.enable = true;
      ftp.enable = true;
      docker.enable = true;
      greenclip.enable = true;
      # Needed occasionally to help the parental units with PC problems
      # teamviewer.enable = true;
    };
    theme.active = "alucard";
  };


  ## Local config
  programs.ssh.startAgent = true;
  services.openssh.startWhenNeeded = true;

  networking = {
    networkmanager.enable = true;
    wireless.enable = true;
    wireless.userControlled.enable = true;
    # The global useDHCP flag is deprecated, therefore explicitly set to false
    # here. Per-interface useDHCP will be mandatory in the future, so this
    # generated config replicates the default behaviour.
    #useDHCP = false;
    #ZFS stuff
    hostId = "4750c034";
    usePredictableInterfaceNames = false; # fuck that systemd shit
  };

  ## Personal backups
  # TODO set it up with a separate HDD
}
