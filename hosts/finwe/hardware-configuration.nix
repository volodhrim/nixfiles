{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [ "${modulesPath}/installer/scan/not-detected.nix" ];

  boot = {
    initrd = {
      availableKernelModules = [
        "ahci"
        "nvme"
        "sd_mod"
        "usb_storage"
        "usbhid"
        "vfio-pci"
        "xhci_pci"
      ];
      kernelModules = [];
      systemd.enable = true;
    };
    blacklistedKernelModules = [
      "nvidia"
      "nouveau"
    ];
    #respectively, the 1080ti and the related audio device
    #extraModprobeConfig = "options vfio-pci ids=10de:1b06,10de:10ef";
    kernelModules = [
      "vfio"
      "vfio_virqfd"
      "vfio_pci"
      "vfio_iommu_type1"
      "fuse"
      "kvm-amd"
    ];
    kernelParams = [
      "amd_iommu=on"
      "pcie_aspm=off"
    ];

  };

  # Modules
  modules.hardware = {
    bluetooth.enable = true;
    audio.enable = true;
    fs = {
      enable = true;
      ssd.enable = true;
    };
    ergodox.enable = true;
    nvidia.enable = true;
    #radeon.enable = true;
    sensors.enable = true;
  };

  # CPU
  nix.settings.max-jobs = lib.mkDefault 16;
  powerManagement.cpuFreqGovernor = "performance";


  # Storage
  fileSystems = {
    "/" = {
      device = "nixpool/root/nixos";
      fsType = "zfs";
    };
    "/home" = {
      device = "nixpool/home";
      fsType = "zfs";
    };
    "/boot" =
    { device = "/dev/disk/by-uuid/F89D-473D";
      fsType = "vfat";
    };
  };


  swapDevices =
    [ { device = "/dev/disk/by-uuid/b521f01f-c3c3-48ae-8535-1ec4065455b5"; }
    ];

  hardware = {
    # high-resolution display
    video.hidpi.enable = lib.mkDefault true;
    cpu.amd.updateMicrocode = true;
    enableRedistributableFirmware = true;
  };
}
