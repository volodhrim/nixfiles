sudo zpool import -f nixpool
zpool list
zfs load-key -a
zfs list
zfs set mountpoint=legacy nixpool/root/nixos
mount -t zfs nixpool/root/nixos /mnt

zfs set mountpoint=legacy nixpool/home
mount -t zfs nixpool/home /mnt/home

mount /dev/disk/by-uuid/F89D-473D /mnt/boot

nixos-enter
