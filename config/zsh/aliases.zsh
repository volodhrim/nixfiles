alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias -- -='cd -'
alias cdg='cd `git rev-parse --show-toplevel`'

alias q=exit
alias clr=clear
alias sudo='sudo '
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias mkdir='mkdir -pv'
alias wget='wget -c'
alias path='echo -e ${PATH//:/\\n}'
alias ports='netstat -tulanp'

alias mk=make
alias gurl='curl --compressed'

alias shutdown='sudo shutdown'
alias reboot='sudo reboot'

# An rsync that respects gitignore
rcp() {
  # -a = -rlptgoD
  #   -r = recursive
  #   -l = copy symlinks as symlinks
  #   -p = preserve permissions
  #   -t = preserve mtimes
  #   -g = preserve owning group
  #   -o = preserve owner
  # -z = use compression
  # -P = show progress on transferred file
  # -J = don't touch mtimes on symlinks (always errors)
  rsync -azPJ \
    --include=.git/ \
    --filter=':- .gitignore' \
    --filter=":- $XDG_CONFIG_HOME/git/ignore" \
    "$@"
}; compdef rcp=rsync
alias rcpd='rcp --delete --delete-after'
alias rcpu='rcp --chmod=go='
alias rcpdu='rcpd --chmod=go='

alias y='xclip -selection clipboard -in'
alias p='xclip -selection clipboard -out'

alias jc='journalctl -xe'
alias sc=systemctl
alias ssc='sudo systemctl'

if command -v exa >/dev/null; then
  alias exa="exa --group-directories-first --git";
  alias l="exa -1";
  alias ll="exa -lag";
  alias la="LC_COLLATE=C exa -la";
fi

autoload -U zmv

take() {
  mkdir "$1" && cd "$1";
}; compdef take=mkdir

zman() {
  PAGER="less -g -I -s '+/^       "$1"'" man zshall;
}

# Create a reminder with human-readable durations, e.g. 15m, 1h, 40s, etc
r() {
  local time=$1; shift
  sched "$time" "notify-send --urgency=critical 'Reminder' '$@'; ding";
}; compdef r=sched

#!/bin/sh
alias cal='cal -3m'
alias k='k -ha'

#work-related
alias grafana-tunnel='ssh -N -L 4200:192.168.106.2:3000 brahin'
alias parsehub='x11docker --home --clipboard x11docker/xfce "./parsehub/parsehub"'
spectraver ()
{
    curl -s "https://${1}.spectra.io/healthcheck" | jq -r '.grape.data.spectra'
}


#alias pastebin="curl -F 'sprunge=<-' http://sprunge.us" # use: cat shit | pastebin
alias asciiflow="bash -c 'cd ~/proj/asciiflow2 && ./compile.sh && python -m http.server'"
alias cd......="cd ../../../../.."
alias cd.....="cd ../../../.."
alias cd....="cd ../../.."
alias cd...="cd ../.."
alias cd..="cd .."
alias ec='emacsclient -c'
alias destroy='shred -u wipe'
alias handbrake='gdb'
alias ragner='ranger'
alias mpva='mpv --vid=no'
alias nocolor='sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g"'
alias noise="play -n synth brownnoise"
alias reset='echo -e "\e[3J" && reset'
alias rot13="tr 'A-Za-z' 'N-ZA-Mn-za-m'"
alias tb="nc termbin.com 9999"
#alias telegram=telegram-desktop
alias xcopy="xsel -b"

# json/yaml conversions
alias json2yml='yq eval -P -'
alias yml2json='yq eval -j -'

#transforms JSON in discrete assignments to make it greppable
alias norg="gron --ungron"
alias ungron="gron --ungron"

#functions
exists(){
    #alternative is command -v
    type "$1" &>/dev/null || return 1 && \
    type "$1" 2>/dev/null | \
    command grep -qv "suffix alias" 2>/dev/null
}

sshk() {
  ssh-keygen -t ed25519 -N '' -f "${HOME}/.ssh/${1}"
}

otprint() {
  oathtool --totp -b "$(cat ~/.config/otp/${1})"
}
otp() {
#  oathtool --totp -b "$(cat ~/.config/otp/${1})" | xsel -b
  otprint ${1} | xsel -b
}
duh() {
  du -ah -d 1 "${1}" | sort -h
}

yt-audio() {
  pushd ~/music
  yt-dlp -x "${1}"
  popd
}

awsp() {
  grep -A 3 "${1}" ~/.aws/credentials
}

awspn() {
  awsp "${1}" | grep role_arn | grep -o '[[:digit:]]*' | xcopy
}

awspr() {
  awsp "${1}" | grep role_arn | grep -o '\/\(.*\)' | tr -d / | xcopy
}

function surround {
  while IFS= read line; do
    echo "${1}${line}${1}"
  done
}

function exportall {
  if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: exportall <path/file_name> where the file contains a list of valid env var assignments"
  else
    if [ -f $1 ] ; then
      set -o allexport
      source $1
      set +o allexport
    else
      echo "$1 - file does not exist"
    fi
  fi
}


function extract {
 if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
 else
    if [ -f $1 ] ; then
        NAME=${1%.*}
        mkdir $NAME && cd $NAME
        case $1 in
          *.tar.bz2)   tar xvjf ../$1    ;;
          *.tar.gz)    tar xvzf ../$1    ;;
          *.tar.xz)    tar xvJf ../$1    ;;
          *.lzma)      unlzma ../$1      ;;
          *.bz2)       bunzip2 ../$1     ;;
          *.rar)       unrar x -ad ../$1 ;;
          *.gz)        gunzip ../$1      ;;
          *.tar)       tar xvf ../$1     ;;
          *.tbz2)      tar xvjf ../$1    ;;
          *.tgz)       tar xvzf ../$1    ;;
          *.zip)       unzip ../$1       ;;
          *.Z)         uncompress ../$1  ;;
          *.7z)        7z x ../$1        ;;
          *.xz)        unxz ../$1        ;;
          *.exe)       cabextract ../$1  ;;
          *)           echo "extract: '$1' - unknown archive method" ;;
        esac
    else
        echo "$1 - file does not exist"
    fi
fi
}

mkcdir ()
{
    mkdir -p -- "$1" &&
      cd -P -- "$1"
}

function _alarm {
  ( \speaker-test --frequency $1 --test sine )&
  pid=$!
  \sleep 0.${2}s
  \kill -9 $pid
}

alias beep='_alarm 2000 100 > /dev/null 2>&1 >/dev/null'

# fun stuff
function maze {
  reset && yes 'printf \\u$[2571+RANDOM%2]' | awk '{system("sleep .005");print}' | bash
}

function cman() {
  env \
    LESS_TERMCAP_mb=$(printf "\e[1;31m") \
    LESS_TERMCAP_md=$(printf "\e[1;31m")
    LESS_TERMCAP_me=$(printf "\e[0m") \
    LESS_TERMCAP_se=$(printf "\e[0m") \
    LESS_TERMCAP_so=$(printf "\e[1;44;30m") \
    LESS_TERMCAP_ue=$(printf "\e[0m") \
    LESS_TERMCAP_us=$(printf "\e[1;32m") \
    PAGER="${commands[less]:-$PAGER}" \
    _NROFF_U=1 \
  man "$@"
}

function otpqrc() {
  if ! command -v qrc &> /dev/null
  then
    go get github.com/vandr0iy/qrc/cmd/qrc
  fi
  qrc "otpauth://totp/${1}:vandr0iy?algorithm=SHA1&digits=6&issuer=aws&period=30&secret=$(cat ~/.config/otp/${1})"
}

portslay () {
    kill -9 `lsof -i tcp:$1 | tail -1 | awk '{ print $2;}'`
}

vcpy () {
  if [[ -n "${1}" ]]; then tempf="${1}"
  else tempf="$(mktemp)"
  fi

  echo "${tempf}"
  vim "${tempf}" && xcopy < "${tempf}"
}

#json2yml () {
#  yq eval -P
#  ruby -ryaml -rjson -e 'puts YAML.dump(JSON.parse(STDIN.read))' < "${1}" > "${1}.yml"
#}
#
#yml2json () {
#  ruby -ryaml -rjson -e 'puts JSON.pretty_generate(YAML.load(ARGF))' < "${1}" > "${1}.json"
#}

writeiso () {
  if [ ! -z $2 ]
  then dest="${2}"
  else dest="/dev/sdd"
  fi

  dd if="${1}" of="${dest}" bs=4M status=progress && sync
}

compress () {
  if [[ -n "${2}" ]]; then tempf="${2}"
  else tempf="$(mktemp).tar.gz"
  fi

  tar -czvf ${tempf} ${1} && echo "${tempf}"
}

countdown () {
  seconds="${1}"; date1=$((`date +%s` + $seconds));
  while [ "$date1" -ge `date +%s` ]; do
    echo -ne "$(date -u --date @$(($date1 - `date +%s` )) +%H:%M:%S)\r";
  done
  beep
}

showpoly () {
  export MONITOR=$(xrandr | grep ' connected' | grep "${1}" | awk '{print $1}')
  killall polybar
  (polybar bigmonitor2 &)
}

parsecert () {
  openssl x509 -in "${1}" -text -noout
}

ipmitun () {
  case ${1} in

  "chaco") ip_addr=192.168.106.23 ;;
  "kaali") ip_addr=192.168.106.24 ;;
  "imilac") ip_addr=192.168.106.21 ;;
  *) >&2 echo "no such server"; return 1 ;;
  esac
  port_opts="$(for i in 80 443 5900 623; do
                 printf '%s 127.0.0.1:%d:%s:%d ' "-L" "${i}" "${ip_addr}" "${i}"
               done)"
  cmd="ssh -i /home/vandr0iy/.ssh/nexellent -N ${port_opts}diadmin@${1}.deep-impact.ch"
  echo "$cmd" && eval "$cmd"
}

set_ip() {
  # DO NOT FORGET THE NETMASK!
  sudo ip addr add $1 broadcast + dev eth0
}

