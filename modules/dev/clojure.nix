# modules/dev/clojure.nix --- https://clojure.org/

{ config, options, lib, pkgs, my, ... }:

with lib;
with lib.my;
let cfg = config.modules.dev.clojure;
in {
  options.modules.dev.clojure = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      clojure
      clojure-lsp
      leiningen
      clj-kondo
    ];
  };
}
