{ config, options, lib, pkgs, my, ... }:

with lib;
with lib.my;
let cfg = config.modules.dev.ops;
in {
  options.modules.dev.ops = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      ansible
      unstable.terraform
      awscli2
      htop
    ];
  };
}
