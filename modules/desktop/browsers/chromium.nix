# modules/browser/chromium.nix ---
#
# Ungoogled chromium. Be evil.

{ options, config, lib, pkgs, inputs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.browsers.chromium;
in {
  options.modules.desktop.browsers.chromium = with types; {
    enable = mkBoolOpt true;
  };

  config = mkIf cfg.enable (mkMerge [
    {
      nixpkgs.overlays = [ inputs.nur.overlay ];
      user.packages = with pkgs; [
        ungoogled-chromium
      ];

      home.programs.chromium = {
        enable = true;
        package = pkgs.ungoogled-chromium;
        extensions =
        let
          createChromiumExtensionFor = browserVersion: { id, sha256, version }:
            {
              inherit id;
              crxPath = builtins.fetchurl {
                url = "https://clients2.google.com/service/update2/crx?response=redirect&acceptformat=crx2,crx3&prodversion=${browserVersion}&x=id%3D${id}%26installsource%3Dondemand%26uc";
                name = "${id}.crx";
                inherit sha256;
              };
              inherit version;
            };
          createChromiumExtension = createChromiumExtensionFor (lib.versions.major pkgs.ungoogled-chromium.version);
        in
        [
          (createChromiumExtension {
            # ublock origin
            id = "cjpalhdlnbpafiamejdnhcphjbkeiagm";
            sha256 = "sha256:1aazal8ycnvf53cvq0nsvkzajw2rc13kbgxg32dnsjdy8x8fp00j";
            version = "1.40.8";
          })
          (createChromiumExtension {
            # dark reader
            id = "eimadpbcbfnmbkopoojfekhnkhdbieeh";
            sha256 = "sha256:00zfs9d68rcqknp0j0hfa4ppyjmdfcij4ks43zga3zj29bwipwl3";
            version = "4.9.43";
          })
          (createChromiumExtension {
            # lastpass
            id = "hdokiejnpimakedhajhdlcegeplioahd";
            sha256 = "sha256:1jwpradbmvywiyciaf6pbpvqf4djd73lrfpycwnvzn54k9qi2kym";
            version = "4.86.0.2";
          })
          (createChromiumExtension {
            # AWS extend switch roles
            id = "jpmkfafbacpgapdghgdpembnojdlgkdl";
            sha256 = "sha256:147d38r9p8245b49d2hh2npsvn85zy54c57l9bnzckmn1bfjal11";
            version = "2.3.0";
          })
        ];
      };
    }
  ]);
}
