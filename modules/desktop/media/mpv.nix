{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let
  cfg = config.modules.desktop.media.mpv;
in {
  options.modules.desktop.media.mpv = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      mpvc  # CLI controller for mpv
      yt-dlp
      tartube-yt-dlp
    ];

    home.programs.mpv = {
      enable = true;
      package = (pkgs.wrapMpv (pkgs.mpv-unwrapped.override {
        vapoursynthSupport = true;
      }) {
        extraMakeWrapperArgs = [
          "--prefix" "LD_LIBRARY_PATH" ":" "${pkgs.vapoursynth-mvtools}/lib/vapoursynth"
        ];
      });
      config = {
        script-opts-append="ytdl_hook-ytdl_path=${pkgs.yt-dlp}/bin/.yt-dlp-wrapped";
      };
    };
  };
}
