{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let
  # obtained via `autorandr --fingerprint`
  Benq4k = "00ffffffffffff0009d11e8045540000141b0104b53c22783f3cd0a7544a9d240e5054a56b80d1c081c081008180a9c0b300010101014dd000a0f0703e8030203500544f2100001a000000ff003835483032383239534c300a20000000fd00303c85853c010a202020202020000000fc0042656e51204c43440a2020202001e5020322f14f90050403020111121314060715161f230907078301000065030c001000023a801871382d40582c4500544f2100001e011d8018711c1620582c2500544f2100009e011d007251d01e206e285500544f2100001e04740030f2705a80b0588a00544f2100001a565e00a0a0a0295030203500544f2100001a0000007e";
  LGNanoIPS = "00ffffffffffff001e6d7f5b060e0500041e0104b53c22789f8cb5af4f43ab260e5054254b007140818081c0a9c0b300d1c08100d1cf28de0050a0a038500830080455502100001a000000fd003090e6e63c010a202020202020000000fc003237474c3835300a2020202020000000ff003030344e54524c39523237300a01ee02031a7123090607e305c000e606050160592846100403011f13565e00a0a0a029503020350055502100001a909b0050a0a046500820880c555021000000b8bc0050a0a055500838f80c55502100001a00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001a";

  notify = "${pkgs.libnotify}/bin/notify-send";
  cfg = config.modules.desktop.media.autorandr;
  #yakuake_autostart = (pkgs.makeAutostartItem { name = "yakuake"; package = pkgs.yakuake; srcPrefix = "org.kde.";  });
  autorandr_autostart = (
    pkgs.makeAutostartItem {
      name = "autorandr";
      package = pkgs.autorandr;
      #srcPrefix = "org.kde.";
    }
  );
in {
  options.modules.desktop.media.autorandr = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    home.programs.autorandr = {
      enable = true;

      hooks = {
        predetect = {};

        preswitch = {};

        postswitch = {
          "notify" = ''
            ${notify} -i display "Display profile" "$AUTORANDR_CURRENT_PROFILE"
          '';
  #
  #        "change-dpi" = ''
  #          case "$AUTORANDR_CURRENT_PROFILE" in
  #            away)
  #              DPI=120
  #              ;;
  #            home)
  #              DPI=96
  #              ;;
  #            *)
  #              ${notify} -i display "Unknown profle: $AUTORANDR_CURRENT_PROFILE"
  #              exit 1
  #          esac
  #
  #          echo "Xft.dpi: $DPI" | ${pkgs.xorg.xrdb}/bin/xrdb -merge
  #        '';
        };
      };

      profiles = {
        "desktop" = {
          fingerprint = {
            DP-4 = Benq4k;
            DP-0 = LGNanoIPS;
          };

          config = {
            DP-4 = {
              enable = true;
              crtc = 1;
              primary = false;
              position = "0x0";
              mode = "2560x1440";
              rate = "59.95";
              rotate = "normal";
            };
            DP-0 = {
              enable = true;
              crtc = 0;
              primary = true;
              position = "0x1440";
              mode = "2560x1440";
              rate = "144.00";
              rotate = "normal";
            };
          };
        };

        #"home" = {
        #  fingerprint = {
        #    HDMI-A-0 = msiOptixId;
        #    eDP = tongfangId;
        #  };

        #  config = {
        #    HDMI-A-0 = {
        #      enable = true;
        #      crtc = 0;
        #      primary = true;
        #      position = "0x0";
        #      mode = "3840x2160";
        #      rate = "30.00";
        #    };
        #    eDP = {
        #      enable = true;
        #      crtc = 1;
        #      position = "0x0";
        #      mode = "1920x1080";
        #      rate = "60.04";
        #      rotate = "normal";
        #    };
        #  };
        #};
      };

    };
  };
}

