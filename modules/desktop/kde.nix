{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.kde;
    configDir = config.dotfiles.configDir;

    fetchPypi = import (builtins.fetchTarball {
      name = "nix-pypi-fetcher";
      url = "https://github.com/DavHau/nix-pypi-fetcher/tarball/389593db84e4fa39ae946fa48cbbeea6dd5dbd90";
      # Hash obtained using `nix-prefetch-url --unpack <url>`
      sha256 = "sha256:1hkzii8b6pwy350cpafjcj2jcqs65zh2pwjj07iisp9yp5ppzjlr";
    });
    konsave = pkgs.python310Packages.buildPythonPackage rec {
      pname = "Konsave";
      version = "2.1.2";
      src = pkgs.python310Packages.fetchPypi {
        inherit pname version;
        sha256 = "sha256-mSXkae4yq5jQBLSFSrjytRaslKhfp1D4J7aZqBJH28Y=";
      };
      doCheck = false;

      propagatedBuildInputs = with pkgs.python310Packages; [
        setuptools-scm
        pyyaml
      ];
    };

in {
  options.modules.desktop.kde = {
    enable = mkBoolOpt false;
  };


  config = mkIf cfg.enable {
    services = {
      xserver = {
        enable = true;
        desktopManager.plasma5.enable = true;
        displayManager = {
          sddm.enable = true;
        };
      };
    };

    boot.plymouth = {
      enable = true;
    };

    environment = {
      systemPackages = with pkgs; [
        plasma5Packages.bismuth
        konsave
        (polybar.override {
          pulseSupport = true;
          nlSupport = true;
        })
      ];
      #shellInit = ''
      #  export GTK_PATH=$GTK_PATH:${pkgs.oxygen_gtk}/lib/gtk-2.0
      #  export GTK2_RC_FILES=$GTK2_RC_FILES:${pkgs.oxygen_gtk}/share/themes/oxygen-gtk/gtk-2.0/gtkrc
      #'';
    };

    # link recursively so other modules can link files in their folders
    #home.configFile = {
    #  "sxhkd".source = "${configDir}/sxhkd";
    #  "bspwm" = {
    #    source = "${configDir}/bspwm";
    #    recursive = true;
    #  };
    #};
  };
}
