#!/usr/bin/env bash

pkill -u $UID -x polybar
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

export PRIMARY_MONITOR=$(xrandr -q | grep ' connected' | grep ' primary' | cut -d' ' -f1)
export OTHER_MONITORS=( $(xrandr -q | grep ' connected' | grep -v ' primary' | cut -d' ' -f1) )
export SECONDARY_MONITOR=${OTHER_MONITORS[1]}

#source $(command -v monitors)

polybar primary >$XDG_DATA_HOME/polybar_primary.log 2>&1 &
if [[ "${SECONDARY_MONITOR}" != "${PRIMARY_MONITOR}" ]]; then
  polybar secondary >$XDG_DATA_HOME/polybar_secondary.log 2>&1 &
fi

echo 'Polybar launched...'
