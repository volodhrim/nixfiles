{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.shell.ranger;
    configDir = config.dotfiles.configDir;
in {
  options.modules.shell.ranger = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      ranger
    ];

    home.configFile = {
      "ranger/rc.conf".source = "${configDir}/ranger/rc.conf";
      "ranger/devicons.py".source = "${configDir}/ranger/devicons.py";
    };
  };
}
