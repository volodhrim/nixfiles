{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.networking;
in {
  options.modules.networking = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      networkmanagerapplet
      winbox
      nmap
      minicom
      ipcalc
    ];
  };
}
