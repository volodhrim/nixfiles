{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.services.greenclip;
in {
  options.modules.services.greenclip = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    services.greenclip.enable = true;
  };
}
