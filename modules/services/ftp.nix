{ options, config, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.services.ftp;
in {
  options.modules.services.ftp = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    networking.firewall = {
      allowedTCPPorts = [ 20 21 ];
      allowedTCPPortRanges = [
        {
          from = 51000;
          to   = 51999;
        }
      ];
    };
    services.vsftpd = {
      enable = true;
      writeEnable = true;
      localUsers = true;
      userlist = [ "vandr0iy" ];
      userlistEnable = true;
      extraConfig = ''
        pasv_enable=Yes
        pasv_min_port=51000
        pasv_max_port=51999
      '';
    };
  };
}
