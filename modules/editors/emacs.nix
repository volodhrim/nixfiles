{ config, lib, pkgs, inputs, ... }:

with lib;
with lib.my;
let cfg = config.modules.editors.emacs;
    configDir = config.dotfiles.configDir;
in {
  options.modules.editors.emacs = {
    enable = mkBoolOpt false;
    doom = {
      enable  = mkBoolOpt true;
      fromSSH = mkBoolOpt false;
    };
  };

  config = mkIf cfg.enable {
    nixpkgs.overlays = [ inputs.emacs-overlay.overlay ];

    user.packages = with pkgs; [
      ## Emacs itself
      binutils              # native-comp needs 'as', provided by this

      # 29 + pgtk + native-comp
      ((emacsPackagesFor emacsPgtkNativeComp).emacsWithPackages (epkgs: [
        epkgs.vterm
      ]))

      ## Doom dependencies
      git
      openssl
      git-lfs             # just throw it away, ffs
      (ripgrep.override {withPCRE2 = true;})
      gnutls              # for TLS connectivity


      ## Optional dependencies
      fd                  # faster projectile indexing
      jq                  # json mangler
      imagemagick         # for image-dired
      (mkIf (config.programs.gnupg.agent.enable)
        pinentry-emacs)   # in-emacs gnupg prompts
      zstd                # for undo-fu-session/undo-tree compression

      ## Module dependencies
      # :checkers spell
      (aspellWithDicts (ds: with ds; [
        en en-computers en-science
      ]))
      # :checkers grammar
      languagetool
      # :tools editorconfig
      editorconfig-core-c # per-project style config
      # :tools lookup & :lang org +roam
      sqlite

      ## lang stuff that don't deserve their own module
      # :lang beancount
      beancount
      unstable.fava
      # :lang latex & :lang org (latex previews)
      texlive.combined.scheme-full
      # :lang markdown
      pandoc
      python310Packages.grip
      mdl
      # :lang nix
      nixfmt
      # :lang org
      scrot
      # :lang plantuml
      plantuml
      graphviz
      jdk
    ];

    env.PATH = [ "$XDG_CONFIG_HOME/emacs/bin" ];

    modules.shell.zsh.rcFiles = [ "${configDir}/emacs/aliases.zsh" ];

    fonts.fonts = [ pkgs.emacs-all-the-icons-fonts ];

    #OLD SPACEMACS CONFIG
    #home.file.".spacemacs".source = "${configDir}/emacs/spacemacs";
    #home.file.".emacs.d" = {
    #  # don't make the directory read only so that impure melpa can still happen
    #  # for now
    #  recursive = true;
    #  source = pkgs.fetchFromGitHub {
    #    owner = "syl20bnr";
    #    repo = "spacemacs";
    #    rev = "9b6083b32d4de500c8728541dd09e196f2e464b6";
    #    # replace with zeroes to update
    #    sha256 = "sha256-J5r4KRgYrTjWNgiA8d/lhYs8o0NL29mpBZbpECe2fD4=";
    #  };
    #};
    system.userActivationScripts = mkIf cfg.doom.enable {
      installDoomEmacs = ''
        if [ ! -d $HOME/.config/emacs ]; then
           ${optionalString cfg.doom.fromSSH ''
              git clone --single-branch --depth=1 git@github.com:hlissner/doom-emacs.git $HOME/.config/emacs
              git clone git@gitlab.com:volodhrim/doomfiles.git $HOME/.config/doom
           ''}
           ${optionalString (cfg.doom.fromSSH == false) ''
              git clone --single-branch --depth=1 https://github.com/hlissner/doom-emacs $HOME/.config/emacs
              git clone https://gitlab.com/volodhrim/doomfiles.git $HOME/.config/doom
           ''}
        fi
      '';
     };
  };
}
