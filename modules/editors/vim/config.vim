filetype off

" General editor options

set autoread                " Auto-update a file that's been edited externally
set backspace=indent,eol,start
set clipboard=unnamedplus   " Copy/Paste to/from clipboard
set cmdheight=1             " Number of screen lines to use for the commandline.
set completeopt+=noinsert   " Select the first item of popup menu automatically without inserting it
set cursorline              " Highlight line cursor is currently on
set fileformats+=mac
set formatoptions=jtcrq     " Sensible default line auto cutting and formatting.
set hidden                  " Allow buffer switching without saving
set hidden                  " Hide files when leaving them.
set iskeyword-=#            " Regard # as a word boundary
set iskeyword-=.            " Regard . as a word boundary
set iskeyword-=_            " Regard _ as a word boundary
set lazyredraw              " Don't update screen while running macros
set linebreak               " Don't cut lines in the middle of a word .
set listchars=nbsp:·,tab:▸\ ,eol:¬ " Invisible characters representation when :set list.
set matchtime=2             " Time during which the matching parenthesis is shown.
set modeline
set noeol
set nospell                 " No spell check, please
set nrformats-=octal
set number relativenumber   " Show line numbers.
set numberwidth=1           " Minimum line number column width.
set shortmess+=filmnrxoOtTs
set showmatch               " Shows matching parenthesis.
set termguicolors
set textwidth=120           " Lines length limit (0 if no limit).
set ttyfast
set visualbell              " No sounds!

set shiftwidth=2 tabstop=2 softtabstop=2 " tab is 2 spaces wide
set expandtab " insert 2 spaces instead of a tab

" Search
set incsearch  " Incremental search.
set ignorecase " Case insensitive.
set smartcase  " Case insensitive if no uppercase letter in pattern, case sensitive otherwise.
set nowrapscan " Don't go back to first match after the last match is found.

" Theme
colorscheme NeoSolarized
set background=dark

" Leader & Shell
let mapleader="\<C-b>"
set shell=/run/current-system/sw/bin/zsh

" Better Unix support
set viewoptions=folds,options,cursor,unix,slash
set encoding=utf-8

" Clear search highlighting
nnoremap <C-z> :nohlsearch<CR>

" Terminal mode exit shortcut
:tnoremap <Esc> <C-\><C-n>

" Fixes broken cursor on Linux
set guicursor=

" Trim whitespace function
function! TrimWhitespace()
    let l:save_cursor = getpos('.')
    %s/\s\+$//e
    call setpos('.', l:save_cursor)
endfun

command! TrimWhitespace call TrimWhitespace() " Trim whitespace with command
autocmd BufWritePre * :call TrimWhitespace()  " Trim whitespace on every save

" Auto-commands Vimscript
augroup vimscript_augroup
  autocmd!
  autocmd FileType vim nnoremap <buffer> <M-z> :execute "help" expand("<cword>")<CR>
augroup END

" Spell check for markdown files
au BufNewFile,BufRead *.md set spell

" Enable clipboard
if has('unnamedplus')
    set clipboard=unnamedplus
else
    set clipboard=unnamed
endif

" Disable the annoying and useless ex-mode
nnoremap Q <Nop>
nnoremap gQ <Nop>

" Disable background (let picom manage it)
hi Normal guibg=NONE ctermbg=NONE
