" Comma get some... sorry.
let mapleader = ' '
let maplocalleader = ' m'

" Trigger to preserve indentation on pastes
set pastetoggle=<F12>
" Turn off search highlighting
noremap <silent> <leader>? :nohlsearch<CR>

" Navigation {{{
    " % matchit shortcut, but only in normal mode!
    nmap <Tab> %

    " Make motions sensitive to wrapped lines
    " Same for 0, home, end, etc
    function! WrapRelativeMotion(key, ...)
        let vis_sel=""
        if a:0
            let vis_sel="gv"
        endif
        if &wrap
            execute "normal!" vis_sel . "g" . a:key
        else
            execute "normal!" vis_sel . a:key
        endif
    endfunction

" Editing {{{
     " Make Y act consistant with C and D
    nnoremap Y y$

     " Enabling repeat in visual mode
    vmap . :normal .<CR>
" }}}

" Buffers {{{
    " Next/prev buffer
    nnoremap ]b :<C-u>bnext<CR>
    nnoremap [b :<C-u>bprevious<CR>
" }}}

" Command {{{
    " Annoying command mistakes <https://github.com/spf13/spf13-vim>
    com! -bang -nargs=* -complete=file E e<bang> <args>
    com! -bang -nargs=* -complete=file W w<bang> <args>
    com! -bang -nargs=* -complete=file Wq wq<bang> <args>
    com! -bang -nargs=* -complete=file WQ wq<bang> <args>
    com! -bang Wa wa<bang>
    com! -bang WA wa<bang>
    com! -bang Q q<bang>
    com! -bang QA qa<bang>
    com! -bang Qa qa<bang>
    " Forget to sudo?
    com! WW w !sudo tee % >/dev/null

    " Shortcuts
    cnoremap ;/ <C-R>=expand('%:p:h').'/'<CR>
    cnoremap ;; <C-R>=expand("%:t")<CR>
    cnoremap ;. <C-R>=expand("%:p:r")<CR>

    " Mimic shortcuts in the terminal
    cnoremap <C-a> <Home>
    cnoremap <C-e> <End>
" }}}

" Plugins {{{
    " bufkill
    nnoremap zx :Bdelete<CR>

       " NERDTree
    map <localleader>\ :NERDTree <C-r>=FindRootDirectory()<CR><CR>
    map <localleader>. :NERDTreeFind<CR>

    " Tabularize
    nmap <leader>= :Tabularize /
    vmap <leader>= :Tabularize /

" }}}
"
" Up/down/left/right {{{
    nnoremap n h|xnoremap n h|onoremap n h|
    nnoremap e j|xnoremap e j|onoremap e j|
    nnoremap i k|xnoremap i k|onoremap i k|
    nnoremap o l|xnoremap o l|onoremap o l|

    nnoremap N H|xnoremap N H|onoremap N H|
    nnoremap E J|xnoremap E J|onoremap E J|
    nnoremap I K|xnoremap I K|onoremap I K|
    nnoremap O L|xnoremap O L|onoremap O L|

    nnoremap h n|xnoremap h n|onoremap h n|
    nnoremap j e|xnoremap j e|onoremap j e|
    nnoremap k i|xnoremap k i|onoremap k i|
    nnoremap l o|xnoremap l o|onoremap l o|

    nnoremap H N|xnoremap H N|onoremap H N|
    nnoremap J E|xnoremap J E|onoremap J E|
    nnoremap K I|xnoremap K I|onoremap K I|
    nnoremap L O|xnoremap L O|onoremap L O|
" }}}
" Window handling {{{
    nnoremap <C-W>n <C-W>h|xnoremap <C-W>n <C-W>h|
    nnoremap <C-W>e <C-W>j|xnoremap <C-W>e <C-W>j|
    nnoremap <C-W>i <C-W>k|xnoremap <C-W>i <C-W>k|
    nnoremap <C-W>o <C-W>l|xnoremap <C-W>o <C-W>l|
" }}}
" Folds, etc. {{{
    nnoremap zn zj|xnoremap zn zj|
    nnoremap ze zk|xnoremap ze zk|
" }}}

    " Normalize all the navigation keys to move by row/col despite any wrapped text
    " noremap j gj
    " noremap k gk
