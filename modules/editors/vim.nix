{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let
  cfg = config.modules.editors.vim;

  overriddenPlugins = with pkgs; [];

  myVimPlugins = with pkgs.vimPlugins; [
    asyncrun-vim            # run async commands, show result in quickfix window
    coc-nvim                # LSP client + autocompletion plugin
    coc-yank                # yank plugin for CoC
    dhall-vim               # Syntax highlighting for Dhall lang
    fzf-vim                 # fuzzy finder
    ghcid                   # ghcid for Haskell
    # lightline-vim           # configurable status line (can be used by coc)
    NeoSolarized            # modern theme with true colors support
    multiple-cursors        # Multiple cursors selection, etc
    neomake                 # run programs asynchronously and highlight errors
    nerdtree                # tree explorer
    nerdtree-git-plugin     # shows files git status on the NerdTree
    quickfix-reflector-vim  # make modifications right in the quickfix window
    rainbow_parentheses-vim # for nested parentheses
    registers-nvim          # display registers content
    vim-airline             # bottom status bar
    vim-airline-themes
    vim-css-color           # preview css colors
    vim-devicons            # dev icons shown in the tree explorer
    vim-easy-align          # alignment plugin
    vim-easymotion          # highlights keys to move quickly
    vim-fugitive            # git plugin
    vim-mergetool           # git mergetool for nvim
    vim-nix                 # nix support (highlighting, etc)
    vim-repeat              # repeat plugin commands with (.)
    #vim-ripgrep             # blazing fast search using ripgrep
    vim-surround            # quickly edit surroundings (brackets, html tags, etc)
    vim-tmux                # syntax highlighting for tmux conf file and more
    vim-which-key           # display possible keybindings of the command you type.
  ] ++ overriddenPlugins;

  baseConfig    = builtins.readFile ./vim/config.vim;
  pluginsConfig = builtins.readFile ./vim/plugins.vim;
  colemakConfig = builtins.readFile ./vim/colemak.vim;
  vimConfig     = baseConfig + pluginsConfig + colemakConfig;

in {
  options.modules.editors.vim = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      gopls
      editorconfig-core-c
      unstable.neovim
    ];


    home.programs.neovim = {
      enable       = true;
      extraConfig  = vimConfig;
      plugins      = myVimPlugins;
      viAlias      = true;
      vimAlias     = true;
      vimdiffAlias = true;
      withNodeJs   = true; # for coc.nvim
      withPython3  = true; # for plugins
      coc = {
      enable = true;

      #package = pkgs.vimUtils.buildVimPluginFrom2Nix {
      #  pname = "coc.nvim";
      #  version = "2022-05-21";
      #  src = pkgs.fetchFromGitHub {
      #    owner = "neoclide";
      #    repo = "coc.nvim";
      #    rev = "791c9f673b882768486450e73d8bda10e391401d";
      #    sha256 = "sha256-MobgwhFQ1Ld7pFknsurSFAsN5v+vGbEFojTAYD/kI9c=";
      #  };
      #  meta.homepage = "https://github.com/neoclide/coc.nvim/";
      #};

      settings = {
        "coc.preferences.rootPatterns" = [ ".rooter_root" ".git" ".hg" ".projections.json" ];
        "explorer.keyMappings.global" = {
          "<cr>" = [
            "expandable?"
            [ "expanded?" "collapse" "expand" ]
            "open"
          ];
        };
        "explorer.file.showHiddenFiles" = true;
        "explorer.git.icon.status.added" = "✚";
        "explorer.git.icon.status.copied" = "➜";
        "explorer.git.icon.status.deleted" = "✖";
        "explorer.git.icon.status.ignored" = "☒";
        "explorer.git.icon.status.mixed" = "✹";
        "explorer.git.icon.status.modified" = "✹";
        "explorer.git.icon.status.renamed" = "➜";
        "explorer.git.icon.status.unmerged" = "═";
        "explorer.git.icon.status.untracked" = "?";
        "explorer.git.showIgnored" = true;
        "explorer.icon.enableNerdfont" = true;
        "explorer.position" = "right";
        "languageserver" = {
          "nix" = {
            "command" = "rnix-lsp";
            "filetypes" = [ "nix" ];
          };
          "racket" = {
            "command" = "racket";
            "args" = [ "--lib" "racket-langserver" ];
            "filetypes" = [ "racket" ];
          };
          "vala" = {
            "command" = "vala-language-server";
            "filetypes" = [ "vala" "genie" ];
          };
        };
      };
      pluginConfig = ''
        " CoC Extensions
        let g:coc_global_extensions=[
            \ 'coc-clangd',
            \ 'coc-css',
            \ 'coc-dictionary',
            \ 'coc-docker',
            \ 'coc-eslint',
            \ 'coc-explorer',
            \ 'coc-go',
            \ 'coc-html',
            \ 'coc-java',
            \ 'coc-java-debug',
            \ 'coc-json',
            \ 'coc-lists',
            \ 'coc-marketplace',
            \ 'coc-omnisharp',
            \ 'coc-prettier',
            \ 'coc-pyright',
            \ 'coc-sh',
            \ 'coc-texlab',
            \ 'coc-toml',
            \ 'coc-tsserver',
            \ 'coc-word',
            \ 'coc-yaml',
            \ ]
        " Smaller updatetime for CursorHold & CursorHoldI
        set updatetime=300

        " don't give |ins-completion-menu| messages.
        set shortmess+=c

        " always show signcolumns
        set signcolumn=auto

        " Some server have issues with backup files, see #649
        set nobackup
        set nowritebackup

        " Better display for messages
        set cmdheight=1

        " Use <cr> for confirm completion, `<C-g>u` means break undo chain at current position.
        " Coc only does snippet and additional edit on confirm.
        " inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

        " Use tab for trigger completion with characters ahead and navigate.
        " inoremap <silent><expr> <TAB>
        "       \ pumvisible() ? "\<C-n>" :
        "       \ <SID>check_back_space() ? "\<TAB>" :
        "       \ coc#refresh()
        inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
        function! s:check_back_space() abort
          let col = col('.') - 1
          return !col || getline('.')[col - 1]  =~# '\s'
        endfunction

        " Remap keys for gotos
        nmap <silent> gd <Plug>(coc-definition)
        nmap <silent> gy <Plug>(coc-type-definition)
        nmap <silent> gi <Plug>(coc-implementation)
        nmap <silent> gr <Plug>(coc-references)

        " Formatting
        vmap <C-F> <Plug>(coc-format-selected)
        xmap <C-F> :call CocAction('format')<CR>
        nmap <C-F> :call CocAction('format')<CR>

        " Hover and rename
        nmap <silent> <F6> <Plug>(coc-rename)
        nnoremap <silent> K :call CocAction('doHover')<CR>

        " Go to symbol in (document|project)
        nmap <silent> S :CocList symbols<CR>

        " CoC Explorer
        function! CocExploreCwd()
            let cwd = substitute(execute(":pwd"), '\n', "", "")
            exe 'CocCommand explorer ' . cwd
        endfunction
        map <S-T> :call CocExploreCwd()<CR>

        " Use D for show documentation in preview window
        nnoremap <silent> D :call <SID>show_documentation()<CR>
        function! s:show_documentation()
          if &filetype == 'vim'
            execute 'h '.expand('<cword>')
          else
            call CocAction('doHover')
          endif
        endfunction

        " Highlight symbol under cursor on CursorHold
        autocmd CursorHold * silent call CocActionAsync('highlight')

        " Remap for rename current word
        nmap <leader>rn <Plug>(coc-rename)

        " Show all diagnostics
        nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
        " Find symbol of current document
        nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
        " Search workspace symbols
        nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
        " Do default action for next item.
        nnoremap <silent> <space>j  :<C-u>CocNext<CR>
        " Do default action for previous item.
        nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
        " Resume latest coc list
        nnoremap <silent> <space>p  :<C-u>CocListResume<CR>
        " Run a commands such as `metals.doctor-run`
        nnoremap <silent> <space>c  :<C-u>CocCommand<CR>

        nnoremap <silent> <M-B> :call CocRequest('metals', 'workspace/executeCommand', { 'command': 'build-import' })<CR>
        "nnoremap <silent> <M-Z> :ccl<CR>

        " COC Snippets

        " Use <C-o> for trigger snippet expand.
        imap <C-o> <Plug>(coc-snippets-expand)

        " Use <C-e> for select text for visual placeholder of snippet.
        vmap <C-e> <Plug>(coc-snippets-select)

        " Use <C-e> for jump to next placeholder, it's default of coc.nvim
        let g:coc_snippet_next = '<c-e>'

        " Use <C-i> for jump to previous placeholder, it's default of coc.nvim
        let g:coc_snippet_prev = '<c-i>'

        " Use <C-e> for both expand and jump (make expand higher priority.)
        imap <C-e> <Plug>(coc-snippets-expand-jump)

      '';
      };

    };
  };
}
