{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.hardware.android;
in {
  options.modules.hardware.android = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    programs.adb.enable = true;
    services.udev.packages = [
      pkgs.android-udev-rules
    ];
    user = {
      extraGroups = [ "adbusers" ];
      packages = [
        pkgs.androidenv.androidPkgs_9_0.platform-tools
      ];
    };
  };
}
