{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.hardware.audio;
in {
  options.modules.hardware.audio = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    # rtkit is optional but recommended
    security.rtkit.enable = true;
    services.pipewire = {
      enable = true;
      alsa = {
        enable = true;
        support32Bit = true;
      };
      pulse.enable = true;
      # If you want to use JACK applications, uncomment this
      #jack.enable = true;

      # use the example session manager (no others are packaged yet so this is enabled by default,
      # no need to redefine it in your config for now)
      #media-session.enable = true;
      config.pipewire = {
        "context.properties" = {
          #"link.max-buffers" = 64;
          "link.max-buffers" = 16; # version < 3 clients can't handle more than this
          "log.level" = 2; # https://docs.pipewire.org/page_daemon.html
          #"default.clock.rate" = 48000;
          #"default.clock.quantum" = 1024;
          #"default.clock.min-quantum" = 32;
          #"default.clock.max-quantum" = 8192;
        };
      };

      media-session.config.bluez-monitor.rules = [
        {
          # Matches all cards
          matches = [ { "device.name" = "~bluez_card.*"; } ];
          actions = {
            "update-props" = {
              "bluez5.reconnect-profiles" = [ "hfp_hf" "hsp_hs" "a2dp_sink" ];
              # mSBC is not expected to work on all headset + adapter combinations.
              "bluez5.msbc-support" = true;
              # SBC-XQ is not expected to work on all headset + adapter combinations.
              "bluez5.sbc-xq-support" = true;
            };
          };
        }
        {
          matches = [
            # Matches all sources
            { "node.name" = "~bluez_input.*"; }
            # Matches all outputs
            { "node.name" = "~bluez_output.*"; }
          ];
          actions = {
            "node.pause-on-idle" = false;
          };
        }
      ];
    };
    user = {
      extraGroups = [ "audio" "realtime" ];
      packages = with pkgs; [
        pavucontrol
        ncpamixer
	pamixer
      ];
    };
  };
}
#    sound.enable = true;
#    hardware.pulseaudio.enable = true;
#    user.packages = with pkgs; [
#      pavucontrol
#    ];
#
#
#    # HACK Prevents ~/.esd_auth files by disabling the esound protocol module
#    #      for pulseaudio, which I likely don't need. Is there a better way?
#    hardware.pulseaudio.configFile =
#      let inherit (pkgs) runCommand pulseaudio;
#          paConfigFile =
#            runCommand "disablePulseaudioEsoundModule"
#              { buildInputs = [ pulseaudio ]; } ''
#                mkdir "$out"
#                cp ${pulseaudio}/etc/pulse/default.pa "$out/default.pa"
#                sed -i -e 's|load-module module-esound-protocol-unix|# ...|' "$out/default.pa"
#              '';
#      in mkIf config.hardware.pulseaudio.enable
#        "${paConfigFile}/default.pa";
#
